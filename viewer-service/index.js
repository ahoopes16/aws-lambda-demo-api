const { httpMethods, paths } = require('./constants');
const util = require('./util');
const handlers = require('./handlers');

exports.handler = async event => {
    const { httpMethod, path } = event;
    let response;

    try {
        switch (true) {
            case httpMethod === httpMethods.GET && path === paths.viewerPath:
                response = await handlers.viewerHandler();
                break;
            case httpMethod === httpMethods.GET && path === paths.healthPath:
                response = await handlers.healthHandler();
                break;
            default:
                console.log(
                    `${httpMethod} ${path} Did not match any path/method options`
                );
                response = util.buildResponse(404, 'Resource Not Found');
        }
    } catch (error) {
        console.error('Something went wrong!', error);
        response = util.buildResponse(500, { error: error.message });
    }

    return response;
};
