const healthHandler = require('./health');
const viewerHandler = require('./viewer');

module.exports = {
    healthHandler,
    viewerHandler,
};
