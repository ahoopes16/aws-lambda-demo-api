const util = require('../util');

const adminHandler = () => {
    return util.buildResponse(200, {
        message:
            'Congratulations! You can access this information because you have the "viewer" role!',
    });
};

module.exports = adminHandler;
