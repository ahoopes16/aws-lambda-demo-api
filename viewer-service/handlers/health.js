const util = require('../util');

const healthHandler = () => {
    return util.buildResponse(200, {
        message: 'Viewer service is up and healthy!',
    });
};

module.exports = healthHandler;
