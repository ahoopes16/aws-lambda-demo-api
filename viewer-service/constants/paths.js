const viewerPrefix = '/viewer';

const viewerPath = viewerPrefix;
const healthPath = `${viewerPrefix}/health`;

module.exports = {
    viewerPath,
    healthPath,
};
