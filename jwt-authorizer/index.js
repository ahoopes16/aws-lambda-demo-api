const jwt = require('jsonwebtoken');

exports.handler = async event => {
    const { methodArn, authorizationToken } = event;
    const policyStatement = {
        Action: 'execute-api:Invoke',
        Effect: 'Deny',
        Resource: methodArn,
    };
    const response = {
        principalId: 'user',
        policyDocument: {
            Version: '2012-10-17',
            Statement: [policyStatement],
        },
        context: {},
    };

    if (!authorizationToken) {
        return response;
    }

    const token = authorizationToken.split(' ')[1];
    if (!token) {
        return response;
    }

    try {
        const payload = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);

        if (
            !payload.role ||
            payload.role.toLowerCase() !==
                process.env.ALLOWED_ROLE.toLowerCase()
        ) {
            return response;
        }

        policyStatement.Effect = 'Allow';
        response.context.email = payload.email;
        response.context.role = payload.role;
        response.context.firstName = payload.firstName;
        response.context.lastName = payload.lastName;

        return response;
    } catch (error) {
        console.error(error);
        return response;
    }
};
