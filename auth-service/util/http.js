const buildResponse = (statusCode, body, headers = {}) => ({
    statusCode,
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        ...headers,
    },
    body: typeof body === 'string' ? body : JSON.stringify(body),
});

module.exports = {
    buildResponse,
};
