const auth = require('./auth');
const http = require('./http');

module.exports = {
    ...auth,
    ...http,
};
