const { tokenService } = require('../services');
const util = require('../util');

const tokenHandler = async body => {
    const { refreshToken } = body;

    if (!refreshToken) {
        return util.buildResponse(400, {
            message: 'refreshToken is a required field',
        });
    }

    const existingToken = await tokenService.findToken(refreshToken);
    if (!existingToken) {
        return util.buildResponse(403, 'Forbidden');
    }

    // Important if we add expirations to refresh tokens
    const verifiedToken = util.verifyToken(refreshToken, 'refresh');
    if (!verifiedToken) {
        return util.buildResponse(403, 'Forbidden');
    }

    const tokenPayload = {
        email: verifiedToken.email,
        firstName: verifiedToken.firstName,
        lastName: verifiedToken.lastName,
        role: verifiedToken.role,
    };

    const accessToken = util.generateToken(tokenPayload, 'access');

    return util.buildResponse(200, {
        accessToken,
        message: 'Token refreshed successfully!',
    });
};

module.exports = tokenHandler;
