const healthHandler = require('./health');
const loginHandler = require('./login');
const logoutHandler = require('./logout');
const registerHandler = require('./register');
const tokenHandler = require('./token');

module.exports = {
    healthHandler,
    loginHandler,
    logoutHandler,
    registerHandler,
    tokenHandler,
};
