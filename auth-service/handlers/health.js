const util = require('../util');

const healthHandler = () => {
    return util.buildResponse(200, {
        message: 'Authentication service is up and healthy!',
    });
};

module.exports = healthHandler;
