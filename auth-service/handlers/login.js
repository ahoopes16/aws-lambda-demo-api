const { tokenService, userService } = require('../services');
const util = require('../util');

const loginHandler = async body => {
    const { email, password } = body;

    if (!email || !password) {
        return util.buildResponse(400, {
            message: 'email and password are required fields',
        });
    }

    const existingUser = await userService.getUserWithEmail(email);
    if (!existingUser) {
        return util.buildResponse(401, {
            message: 'Email or password is incorrect',
        });
    }

    const passwordMatches = util.comparePassword(
        password,
        existingUser.password
    );
    if (!passwordMatches) {
        return util.buildResponse(401, {
            message: 'Email or password is incorrect',
        });
    }

    const tokenPayload = {
        email: existingUser.email,
        firstName: existingUser.first_name,
        lastName: existingUser.last_name,
        role: existingUser.role,
    };

    const accessToken = util.generateToken(tokenPayload, 'access');
    const refreshToken = util.generateToken(tokenPayload, 'refresh');

    await tokenService.saveRefreshTokenForUser(refreshToken, existingUser.id);

    return util.buildResponse(200, {
        accessToken,
        refreshToken,
        message: 'Login successful!',
    });
};

module.exports = loginHandler;
