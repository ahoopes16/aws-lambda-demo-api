const { tokenService } = require('../services');
const util = require('../util');

const logoutHandler = async body => {
    const { refreshToken } = body;

    if (!refreshToken) {
        return util.buildResponse(400, {
            message: 'refreshToken is a required field',
        });
    }

    await tokenService.deleteToken(refreshToken);

    return util.buildResponse(200, { message: 'Logout successful!' });
};

module.exports = logoutHandler;
