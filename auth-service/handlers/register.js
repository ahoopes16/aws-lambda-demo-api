const { userService } = require('../services');
const util = require('../util');

const registerHandler = async body => {
    const { email, password, role, firstName, lastName } = body;

    if (!email || !password || !role || !firstName || !lastName) {
        return util.buildResponse(400, {
            message:
                'email, password, role, firstName, and lastName are required fields',
        });
    }

    const emailAlreadyExists = await userService.getUserWithEmail(email);
    if (emailAlreadyExists) {
        return util.buildResponse(400, {
            message: `A user with the email "${email}" already exists`,
        });
    }

    await userService.createUser(body);

    return util.buildResponse(201, 'Created');
};

module.exports = registerHandler;
