const { Client } = require('pg');

const query = async (queryString, parameters = []) => {
    const client = new Client({
        database: process.env.RDS_DB_NAME,
        host: process.env.RDS_HOSTNAME,
        password: process.env.RDS_PASSWORD,
        port: process.env.RDS_PORT,
        user: process.env.RDS_USERNAME,
    });
    await client.connect();

    const response = await client.query(queryString, parameters);

    await client.end();

    return response;
};

module.exports = {
    query,
};
