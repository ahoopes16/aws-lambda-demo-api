const tokenService = require('./token.service');
const userService = require('./user.service');

module.exports = {
    tokenService,
    userService,
};
