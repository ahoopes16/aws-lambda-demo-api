const db = require('../database');
const util = require('../util');

const createUser = userInformation => {
    const { email, password, role, firstName, lastName } = userInformation;

    return db.query(
        'INSERT INTO public.users (email, password, role, first_name, last_name) VALUES ($1::text, $2::text, $3::text, $4::text, $5::text)',
        [email, util.hashPassword(password), role, firstName, lastName]
    );
};

const getUserWithEmail = async email => {
    const response = await db.query(
        'SELECT * FROM public.users WHERE email = $1::text',
        [email]
    );

    if (!response || !response.rows || !response.rows.length) {
        return null;
    }

    return response.rows[0];
};

module.exports = {
    createUser,
    getUserWithEmail,
};
