const db = require('../database');

const deleteToken = token => {
    return db.query('DELETE FROM public.tokens WHERE token = $1::text', [
        token,
    ]);
};

const findToken = async token => {
    const response = await db.query(
        'SELECT * FROM public.tokens WHERE token = $1::text',
        [token]
    );

    if (!response || !response.rows || !response.rows.length) {
        return null;
    }

    return response.rows[0];
};

const saveRefreshTokenForUser = async (token, userId) => {
    const existingTokenResponse = await db.query(
        'SELECT * FROM public.tokens WHERE user_id = $1',
        [userId]
    );
    const existingToken = existingTokenResponse.rows[0];

    if (existingToken) {
        return db.query('UPDATE public.tokens SET token = $1 WHERE id = $2', [
            token,
            existingToken.id,
        ]);
    }

    return db.query(
        'INSERT INTO public.tokens (token, user_id) VALUES ($1::text, $2)',
        [token, userId]
    );
};

module.exports = {
    deleteToken,
    findToken,
    saveRefreshTokenForUser,
};
