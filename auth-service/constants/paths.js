const authPrefix = '/auth';

const healthPath = `${authPrefix}/health`;
const loginPath = `${authPrefix}/login`;
const logoutPath = `${authPrefix}/logout`;
const registerPath = `${authPrefix}/register`;
const tokenPath = `${authPrefix}/token`;

module.exports = {
    healthPath,
    loginPath,
    logoutPath,
    registerPath,
    tokenPath,
};
