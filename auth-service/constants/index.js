const httpMethods = require('./http-methods');
const paths = require('./paths');

module.exports = {
    httpMethods,
    paths,
};
