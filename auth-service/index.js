const { httpMethods, paths } = require('./constants');
const util = require('./util');
const handlers = require('./handlers');

exports.handler = async event => {
    const { body, httpMethod, path } = event;
    let response;

    try {
        switch (true) {
            case httpMethod === httpMethods.GET && path === paths.healthPath:
                response = await handlers.healthHandler();
                break;
            case httpMethod === httpMethods.POST && path === paths.loginPath:
                response = await handlers.loginHandler(JSON.parse(body));
                break;
            case httpMethod === httpMethods.POST && path === paths.logoutPath:
                response = await handlers.logoutHandler(JSON.parse(body));
                break;
            case httpMethod === httpMethods.POST && path === paths.registerPath:
                response = await handlers.registerHandler(JSON.parse(body));
                break;
            case httpMethod === httpMethods.POST && path === paths.tokenPath:
                response = await handlers.tokenHandler(JSON.parse(body));
                break;
            default:
                console.log(
                    `${httpMethod} ${path} Did not match any path/method options`
                );
                response = util.buildResponse(404, 'Resource Not Found');
        }
    } catch (error) {
        console.error('Something went wrong!', error);
        response = util.buildResponse(500, { error: error.message });
    }

    return response;
};
