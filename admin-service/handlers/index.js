const adminHandler = require('./admin');
const healthHandler = require('./health');

module.exports = {
    adminHandler,
    healthHandler,
};
