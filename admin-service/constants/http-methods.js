const GET = 'GET';
const DELETE = 'DELETE';
const OPTIONS = 'OPTIONS';
const PATCH = 'PATCH';
const POST = 'POST';

module.exports = {
    GET,
    DELETE,
    OPTIONS,
    PATCH,
    POST,
};
