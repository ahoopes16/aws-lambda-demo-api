const adminPrefix = '/admin';

const adminPath = adminPrefix;
const healthPath = `${adminPrefix}/health`;

module.exports = {
    adminPath,
    healthPath,
};
